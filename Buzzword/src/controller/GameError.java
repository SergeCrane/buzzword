package controller;

/**
 * @author Serge Crane
 */
public class GameError extends Error {

    public GameError(String message) {
        super(message);
    }
}
