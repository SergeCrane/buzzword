package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import data.Account;
import data.GameData;
import gui.Cell;
import gui.Workspace;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import propertymanager.PropertyManager;
import sun.misc.BASE64Encoder;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javafx.util.Duration;
import java.util.*;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Serge
 */
public class BuzzwordController implements FileController {

    public enum GameState {
        LOGGED_OUT,
        ACCOUNTS_INITIALIZED,
        MAIN_MENU,
        GAMEPLAY,
        LEVEL_SELECT
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Button      gameButton;  // shared reference to the "start game" button
    private File        workFile;

    public  int         numOfEmptyCircles;
    public  static      ArrayList<String> wordsOnGrid = new ArrayList();
    public  static      Random random = new Random();
    private static final int FIRST_WORD_MAX_LENGTH = 10;


    static final int    START_TIME = 15;
    int                 timeRemaining = START_TIME;
    Timeline            timeline;
    IntegerProperty     timeSeconds = new SimpleIntegerProperty(START_TIME);

    public String       currentlySelectedWord = "";
    public int          currentScore;
    private boolean     gameOn = true;
    public String userAccountName;


    public Thread gameThread;

    public BuzzwordController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public BuzzwordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.LOGGED_OUT;
    }

    public void start() {
        PropertyManager propertyManager = PropertyManager.getManager();
        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        workFile = new File( targetPath.toString() + "/save.json");
        gamedata = (GameData) appTemplate.getDataComponent();
        // see if any previous saved files found
        try {
            loadAccounts(workFile);
            setGameState(GameState.ACCOUNTS_INITIALIZED);
        } catch (IOException e) {
            gamedata.init();
            System.out.println("No accounts were found, new file was made.");
        }
    }

    public void generateGrid(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        String currentWord = gamedata.getStartingWord();

        boolean keepGoing = true;
        int counter = 0;
        while (keepGoing) {
            try {
                placeWord(currentWord);
                wordsOnGrid.add(currentWord);      // adding to list of words on the board for score keeping
                keepGoing = false;
            } catch (Exception exception) {
                workspace.clearGrid();
                System.out.println("Try " + counter++);
            }
        }

        // filling rest of grid randomly
        generateRandom();

        // Solve grid
        workspace.initCells();
        solveGrid(3);

        for (String word: wordsOnGrid){
            System.out.println("Word is in grid: " + word);
        }
        timeSeconds = new SimpleIntegerProperty(START_TIME);
        workspace.timerLabel.textProperty().bind(timeSeconds.asString());

        Thread timerThread = new Thread(() -> {
            startTimer();

            while(timeRemaining > 0){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                timeRemaining = timeSeconds.getValue();
                String word = gamedata.getWordFromUserInput();
                if(word != null && word.length() >= 1)
                    currentlySelectedWord = word;
            }
        });
        timerThread.start();

        gameThread = new Thread(() -> {
            while(gameOn) {
                try {
                    Thread.sleep(1000);
                    play();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        gameThread.start();
    }

    public void play() {
        //setGameState(GameState.GAMEPLAY);

        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        int targetScore = gamedata.getTargetScoreByLevel();

        //Platform.runLater(() ->
                currentScore = workspace.grabScoreFromLabel();
        //);

        if (timeRemaining == 0) {
            if (currentScore > targetScore) {
                gameOn = false;


                Platform.runLater(() -> {
                    pauseTimer();
                    revealScoreboard();
                    workspace.displayWin();

                    // Save
                    try {
                        //System.out.println("Trying to save score and level");
                        gamedata.updateLevelProgress(workspace.currentMode);
                        gamedata.checkAndSetBestForLevel(workspace.currentMode, currentScore);

                        save(workFile.toPath());
                    } catch (IOException e) {
                        System.out.println("Error writing to file using json");
                    }
                    // Update GUI to go to next level

                });
            } else {
                gameOn = false;
                workspace.getGridPane().setDisable(true);
                Platform.runLater(() -> {
                    revealScoreboard();
                    YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
                    yesNoCancelDialog.show("Fail", "Your personal best for this level is: " +
                            gamedata.getBestForLevel(workspace.currentMode) +
                            "\n Would you like to try again?");

                    workspace.displayLoss();
                    workspace.updateTotal(true);
                    workspace.setPauseButtonDisable(true);

                    if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                        setGameState(GameState.GAMEPLAY);
                        workspace.reinitialize();
                        workspace.getGridPane().setDisable(false);
                    } else {
                        yesNoCancelDialog.close();
                    }

                });
            }
        }
    }
    public int calculatePoints(String word){
        return word.length() * gamedata.getPointsPerLetter();
    }
    public void checkWordValidity(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        System.out.println("Checking word : " + currentlySelectedWord);
        if(wordsOnGrid.contains(currentlySelectedWord)){
            if(!workspace.getScoreboardAsArrayList().contains(currentlySelectedWord)) {
                System.out.println("Word is valid!");
                workspace.addWordToScoreboard(currentlySelectedWord, calculatePoints(currentlySelectedWord));
                workspace.updateTotal(false);

            }
        }

    }

    public void revealScoreboard(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        wordsOnGrid.removeAll(workspace.getScoreboardAsArrayList());

        for(String word : wordsOnGrid){
            Label wordLabel = new Label(word);
            wordLabel.setStyle("-fx-text-fill: Red");
            int scoreValue = calculatePoints(word);
            Label scoreLabel = new Label("" + scoreValue);
            scoreLabel.setStyle("-fx-text-fill: Red");
            workspace.addLabelsToScoreboard(wordLabel, scoreLabel);
        }
    }

    public int calculateTotalScorePossible(){
        StringBuffer allwords = new StringBuffer();

        for(String s : wordsOnGrid){
            allwords.append(s);
        }

        return allwords.toString().toCharArray().length * gamedata.getPointsPerLetter();
    }
    /*
    Takes in a min wordLength to calculate
     */
    private void solveGrid(int minWordLength){
        if(minWordLength <= 0){
            System.out.println("Invalid word length parameter");
            return;
        }

        Workspace                   workspace = (Workspace) appTemplate.getWorkspaceComponent();
        Set<String>                 letters = getLettersOnBoard().keySet();
//        Iterator<String>            lettersIterator = letters.iterator();

        HashSet<String>             wordList = gamedata.getWordSet();
        Iterator<String>            wordIterator = wordList.iterator();

        while(wordIterator.hasNext()) {
            String currentWord = wordIterator.next();               // word to check
            if(currentWord.length() >= minWordLength){
                if(letters.contains(""+currentWord.charAt(0))){            // checks if the first letter is on the board
                    if(checkWord(currentWord)){
                        if(!wordsOnGrid.contains(currentWord)) {
                            wordsOnGrid.add(currentWord);
                        }
                    }
                } else {
                    //System.out.println("First letter of " + currentWord + " isn't even on the board");
                }
            } else {
                //System.out.println(currentWord + " is too short");
            }
        }
    }

    public boolean checkWord(String word){
        Workspace           workspace = (Workspace) appTemplate.getWorkspaceComponent();
        char[]              letters = word.toCharArray();
        Stack<StackPane>    possibleStartPanes = workspace.getStackPanesByText("" + letters[0]);
        boolean wordExists = false;

        Cell startingCell;
        // now we must check paths starting from every letter occurence
        while(!possibleStartPanes.isEmpty()){
            startingCell = (Cell) possibleStartPanes.pop();
            startingCell.setVisited(true);

            int charIndex = 1;
            wordExists = workspace.tracePath(charIndex, letters, startingCell);
            workspace.unvisitCells();
            workspace.removeHighlighting();

            if(wordExists) {
                break;
            }
        }
        return wordExists;
    }


    public void placeWord(String wordToPlace){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        int[]       coords;
        char[]      currentWord = wordToPlace.toCharArray();
        String      letterToPlace = null;

        System.out.println("current word is -> " + wordToPlace);


        // Starting from the first letter to place
        letterToPlace = "" + currentWord[0];
        if(wordsOnGrid.isEmpty()){
            coords = findRandomEmpty();
        } else {
            HashMap<String, ArrayList> lettersOnBoard = getLettersOnBoard();
            if(lettersOnBoard.containsKey(letterToPlace)){  // if board has current letter already on board
                ArrayList possibleCoords = lettersOnBoard.get(letterToPlace);

                Random random = new Random();
                if(random.nextBoolean() && !possibleCoords.isEmpty())
                    possibleCoords.get(random.nextInt(possibleCoords.size()));
            }
            coords = findRandomEmpty();
        }
        workspace.setCircleText(coords[0], coords[1], letterToPlace);


        // 2nd letter onward
        int lastCol;
        int lastRow;
        for (int i = 1; i < currentWord.length; i++){
            lastCol = coords[0];
            lastRow = coords[1];
            letterToPlace = "" + currentWord[i];
            //System.out.println("The letter to be placed is: " + letterToPlace);
            // check surrounding boxes

            //last stackpane's coordinates
            coords = findNextSuitable(lastCol, lastRow, letterToPlace);

            while (!isCircleEmpty(coords[0], coords[1])){
                coords = findNextSuitable(lastCol, lastRow, letterToPlace);
            }

            workspace.setCircleText(coords[0], coords[1], letterToPlace);
        }
    }


    private void generateRandom(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        //char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] alphabet = ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "bbbbbbbbbbbbbbbbbbbbbb" +
                "ccccccccccccccccccccccccccccccccccccccc" +
                "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd" +
                "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee" +
                "fffffffffffffffffffffffffffffffff" +
                "ggggggggggggggggggggggggggggg" +
                "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh" +
                "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii" +
                "jjj" +
                "kkkkkkkkkk" +
                "lllllllllllllllllllllllllllllllllllllllllllllllllllllllll" +
                "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm" +
                "nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn" +
                "ooooooooooooooooooooooooooooooooooooooooooooooooooooooo" +
                "pppppppppppppppppppppppppp" +
                "qqq" +
                "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr" +
                "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss" +
                "ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt" +
                "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu" +
                "vvvvvvvvvvvvvvv" +
                "wwwwwwwwwwwwwwwwwwwwwwwwwwwwww" +
                "xxxx" +
                "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" +
                "zz").toCharArray();

        for(int col = 0; col < 4; col++){
            for(int row = 0; row < 4; row++){
                if(isCircleEmpty(col, row)){
                    String toAdd = "" + alphabet[random.nextInt(alphabet.length)];
                    workspace.setCircleText(col, row, toAdd);
                }
            }
        }
    }

    private boolean hasSuitableLetter(int col, int row, String currentLetter){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        StackPane pane = workspace.getStackPaneByColRow(col, row);
        String s = workspace.getTextByPane(pane);
        
        //System.out.println("This cell's letter is: " + s);
        if (s.equals(currentLetter))
            return true;
        else
        return false;
    }

    private int[] findNextSuitable(int prevCol, int prevRow, String currentLetter){
        int testCol, testRow;

        ArrayList emptyCols = new ArrayList();
        ArrayList emptyRows = new ArrayList();

        // -1 to both
        testCol = prevCol - 1;
        testRow = prevRow - 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // -1 to col, row stay
        testCol = prevCol - 1;
        testRow = prevRow;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // -1 to col, +1 to row
        testCol = prevCol - 1;
        testRow = prevRow + 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // col stay, row -1
        testCol = prevCol;
        testRow = prevRow - 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // col stay, row +1
        testCol = prevCol;
        testRow = prevRow + 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // col + 1, row -1
        testCol = prevCol + 1;
        testRow = prevRow - 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // col + 1, row stay
        testCol = prevCol + 1;
        testRow = prevRow;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        // col + 1, row + 1
        testCol = prevCol + 1;
        testRow = prevRow + 1;
        if (testCol >= 0 && testCol < 4 && testRow >= 0 && testRow < 4) {
            if(isCircleEmpty(testCol, testRow) || hasSuitableLetter(testCol, testRow, currentLetter)){
                emptyCols.add(testCol);
                emptyRows.add(testRow);
            }
        }

        if(emptyCols.size() > 0 && emptyRows.size() > 0) {
            int col = (int) emptyCols.get(random.nextInt(emptyCols.size()));
            int row = (int) emptyRows.get(random.nextInt(emptyRows.size()));
            int[] testCoords = new int[] {col, row};
            int[] nextCoords = testCoords;
            if(nextCoords[0] == -1 || nextCoords[1] == -1){
                while(nextCoords[0] == -1 || nextCoords[1] == -1){
                    nextCoords = findNextSuitable(prevCol, prevRow, currentLetter);
                }
                return nextCoords;
            }
            return testCoords;
        } else {
            System.out.println("Returning -1s for coords");
            return new int[] {-1, -1};
        }
    }

    private boolean isCircleEmpty(int col, int row){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        StackPane toCheck = workspace.getStackPaneByColRow(col, row);
        String circleText = workspace.getTextByPane(toCheck);

        if (!circleText.equals(" ")){
            //System.out.println(col + " " + row + " has text: " + circleText);
            return false;
        }
        //System.out.println("Returning true, should be empty: " + circleText + " <---");
        return true;
    }

    private int[] findRandomEmpty(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        int col = random.nextInt(4);
        int row = random.nextInt(4);
        StackPane randomPane = workspace.getStackPaneByColRow(col, row);
        // get text at the cell
        String circleText = workspace.getTextByPane(randomPane);

        // while we are on a circle thats not empty, find new circle
        if(numOfEmptyCircles == 0){
            // there are no more empty circles, grid is full
            System.out.println("GRID IS FULL!");
        } else {
            while (!circleText.equals(" ")){
                col = random.nextInt(4);
                row = random.nextInt(4);
                randomPane = workspace.getStackPaneByColRow(col, row);
                circleText = workspace.getTextByPane(randomPane);
            }
        }
        return new int[] {col, row};
    }

    public HashMap<String, ArrayList> getLettersOnBoard(){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        HashMap<String, ArrayList> stringHashMap = new HashMap<>();
        ArrayList arrayList = null;
        for (int col = 0; col < 4; col++){
            for (int row = 0; row < 4; row++){
                StackPane tempPane = workspace.getStackPaneByColRow(col, row);
                String s = workspace.getTextByPane(tempPane);
                if (!s.equals(" ")) {
                    // check if key is already in the map
                    if(stringHashMap.containsKey(s)){
                        arrayList = stringHashMap.get(s);
                        arrayList.add(new int[]{col, row});
                        stringHashMap.put(s, arrayList);
                    } else {
                        arrayList = new ArrayList();
                        arrayList.add(new int[]{col, row});
                        stringHashMap.put(s, arrayList);
                    }
                }
            }
        }
        return stringHashMap;
    }

    public void setSelection(StringBuffer stringBuffer){
        currentlySelectedWord = stringBuffer.toString();
        gamedata.setWordFromUserInput(currentlySelectedWord);
    }

    public void loadWords(String category){
        PropertyManager propertyManager = PropertyManager.getManager();
        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        File words = new File(appDirPath.toString() + "\\resources\\words\\" + category.toLowerCase() + ".txt");
        System.out.println(words.toPath().toString());
        try {
            List<String> allWords = Files.readAllLines(words.toPath(), Charset.defaultCharset());
            gamedata.setWordSet(allWords);

            String firstWord = allWords.get(new Random().nextInt(allWords.size()));
            firstWord.replaceAll("[^A-Za-z]", "");
            while(firstWord.length() > FIRST_WORD_MAX_LENGTH){
                firstWord =allWords.get(new Random().nextInt(allWords.size()));
            }
            gamedata.setStartingWord(firstWord.toLowerCase());


        } catch (IOException e) {
            System.out.println("Failed loading category file with words");
        }
    }



    // Timer
    public void startTimer(){
        if (timeline != null)
            timeline.stop();
        Platform.runLater(() -> timeSeconds.set(START_TIME));
        timeline = new Timeline();

        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(START_TIME+1),
                        new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();

    }

    public void pauseTimer(){
        timeline.pause();
    }

    public void resumeTimer(){
        timeline.play();
    }
    // /Timer


    public void createNewProfile(String u, String p){
        Account acc = new Account(u, p, new int[]{1,1,1,1 });
        gamedata.getAccountsMap().put(u, acc);
        try {
            save(workFile.toPath());
        } catch (IOException e) {
            System.out.println("Error writing to file using json");
        }
    }

    public void authenticate(String u, String oldP) {
        String p = digestPassword(oldP);
        if (u == null || p == null) {
            System.out.println("Null username or password");
            return;
        }

        Account account = gamedata.getAccountsMap().get(u);
        if (account == null) {
            System.out.println("No such username stored in accountsMap");
        } else {
            if (p.equals(account.getPassword())) {
                System.out.println("Log in successful. Welcome " + account.getUsername());
                // if successfully authenticated, update gamedata with all the info
                gamedata.setUserAccount(account);
                gamedata.setCurrentLevelProgress(account.getLevelProgress());
                // then open the main menu
                setGameState(GameState.MAIN_MENU);
                Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
                workspace.openMainMenu();               // call workspace to open main menu
            } else {
                System.out.println("loginMap in gamedata is null");
            }
        }
    }

    public String digestPassword(String pw){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(pw.getBytes());

            byte[] byteArray = messageDigest.digest();
            BASE64Encoder encoder = new BASE64Encoder();
            return encoder.encode(byteArray);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "Error in digesting password";

    }

    void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
     }

    private void loadAccounts(File source) throws IOException {
        ObjectMapper om = new ObjectMapper();
        LinkedHashMap newAccountMap = om.readValue(source, new TypeReference<LinkedHashMap<String, Account>>(){});
        gamedata.setAccountsMap(newAccountMap);
    }

    public void promptLogout() {
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();

        yesNoCancelDialog.show("Logout", "Are you sure you want to log out?");

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            setGameState(GameState.LOGGED_OUT);
            workspace.reinitialize();
        } else {
            yesNoCancelDialog.close();
        }
    }







    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
        System.out.println("The current gamestate is: " + this.gamestate);
    }

    public GameState getGamestate() {
        return this.gamestate;
    }


    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);

    }







    @Override
    public void handleNewRequest() {
    }

    @Override
    public void handleSaveRequest() throws IOException {
        save(workFile.toPath());
    }

    @Override
    public void handleLoadRequest() throws IOException {
    }

    @Override
    public void handleExitRequest() {

    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }





    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        //workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
