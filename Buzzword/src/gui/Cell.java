package gui;

import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Serge on 12/10/2016.
 */
public class Cell extends StackPane {
    private Cell top, btm, left, right, topLeft, topRight, btmLeft, btmRight;        // all the neighboring nodes
    private int col, row;
    private boolean visited;
    private String letter;

    public Cell() {
        super();
        visited = false;
    }

    public Cell(StackPane pane) {
        super(pane);
    }

    public ArrayList<int[]> getValidNeighbors(String letterToLookFor){
        ArrayList<int[]> toReturn = new ArrayList<>();
        int[] possibleCoords;
        if(topLeft != null)
            if (topLeft.getLetter().equals(letterToLookFor) && !topLeft.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = topLeft.col;
                possibleCoords[1] = topLeft.row;
                toReturn.add(possibleCoords);
            }
        if(top != null)
            if (top.getLetter().equals(letterToLookFor) && !top.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = top.col;
                possibleCoords[1] = top.row;
                toReturn.add(possibleCoords);
            }
        if(topRight != null)
            if (topRight.getLetter().equals(letterToLookFor) && !topRight.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = topRight.col;
                possibleCoords[1] = topRight.row;
                toReturn.add(possibleCoords);
            }
        if(left != null)
            if (left.getLetter().equals(letterToLookFor) && !left.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = left.col;
                possibleCoords[1] = left.row;
                toReturn.add(possibleCoords);
            }
        if(right != null)
            if (right.getLetter().equals(letterToLookFor) && !right.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = right.col;
                possibleCoords[1] = right.row;
                toReturn.add(possibleCoords);
            }
        if(btmLeft != null)
            if (btmLeft.getLetter().equals(letterToLookFor) && !btmLeft.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = btmLeft.col;
                possibleCoords[1] = btmLeft.row;
                toReturn.add(possibleCoords);
            }
        if(btm != null)
            if (btm.getLetter().equals(letterToLookFor) && !btm.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = btm.col;
                possibleCoords[1] = btm.row;
                toReturn.add(possibleCoords);
            }
        if(btmRight != null)
            if (btmRight.getLetter().equals(letterToLookFor) && !btmRight.visited) {
                possibleCoords = new int[2];
                possibleCoords[0] = btmRight.col;
                possibleCoords[1] = btmRight.row;
                toReturn.add(possibleCoords);
            }

        if(toReturn.isEmpty()) {
            possibleCoords = new int[2];
            possibleCoords[0] = -1;
            possibleCoords[1] = -1;
            toReturn.add(possibleCoords);
            return toReturn;
        }
        return toReturn;
    }

    public ArrayList<int[]> getValidNeighborsNoVisits(String letterToLookFor){
        ArrayList<int[]> toReturn = new ArrayList<>();
        int[] possibleCoords;
        if(topLeft != null)
            if (topLeft.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = topLeft.col;
                possibleCoords[1] = topLeft.row;
                toReturn.add(possibleCoords);
            }
        if(top != null)
            if (top.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = top.col;
                possibleCoords[1] = top.row;
                toReturn.add(possibleCoords);
            }
        if(topRight != null)
            if (topRight.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = topRight.col;
                possibleCoords[1] = topRight.row;
                toReturn.add(possibleCoords);
            }
        if(left != null)
            if (left.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = left.col;
                possibleCoords[1] = left.row;
                toReturn.add(possibleCoords);
            }
        if(right != null)
            if (right.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = right.col;
                possibleCoords[1] = right.row;
                toReturn.add(possibleCoords);
            }
        if(btmLeft != null)
            if (btmLeft.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = btmLeft.col;
                possibleCoords[1] = btmLeft.row;
                toReturn.add(possibleCoords);
            }
        if(btm != null)
            if (btm.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = btm.col;
                possibleCoords[1] = btm.row;
                toReturn.add(possibleCoords);
            }
        if(btmRight != null)
            if (btmRight.getLetter().equals(letterToLookFor)) {
                possibleCoords = new int[2];
                possibleCoords[0] = btmRight.col;
                possibleCoords[1] = btmRight.row;
                toReturn.add(possibleCoords);
            }

        if(toReturn.isEmpty()) {
            possibleCoords = new int[2];
            possibleCoords[0] = -1;
            possibleCoords[1] = -1;
            toReturn.add(possibleCoords);
            return toReturn;
        }
        return toReturn;
    }
    public HashMap<String, StackPane> getAllVisitedNeighbors(){
        HashMap<String, StackPane> neighbors = new HashMap<>();
        if(this.getTopLeft() != null && this.getTopLeft().isVisited())
            neighbors.put("topLeft", this.getTopLeft());
        if(this.getTop() != null && this.getTop().isVisited())
            neighbors.put("top", this.getTop());
        if(this.getTopRight() != null && this.getTopRight().isVisited())
            neighbors.put("topRight", this.getTopRight());
        if(this.getRight() != null && this.getRight().isVisited())
            neighbors.put("right", this.getRight());
        if(this.getBtmRight() != null && this.getBtmRight().isVisited())
            neighbors.put("btmRight", this.getBtmRight());
        if(this.getBtm() != null && this.getBtm().isVisited())
            neighbors.put("btm", this.getBtm());
        if(this.getBtmLeft() != null && this.getBtmLeft().isVisited())
            neighbors.put("btmLeft", this.getBtmLeft());
        if(this.getLeft() != null && this.getLeft().isVisited())
            neighbors.put("Left", this.getLeft());

        return neighbors;
    }
    public boolean hasValidNeighbor(String letterToLookFor){
        ArrayList<int[]> tmp = getValidNeighbors(letterToLookFor);
        if(!tmp.isEmpty())
            if(tmp.get(0)[0] != -1)
                return true;
        return false;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Cell getTop() {
        return top;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setTop(Cell top) {
        this.top = top;
    }

    public Cell getBtm() {
        return btm;
    }

    public void setBtm(Cell btm) {
        this.btm = btm;
    }

    public Cell getLeft() {
        return left;
    }

    public void setLeft(Cell left) {
        this.left = left;
    }

    public Cell getRight() {
        return right;
    }

    public void setRight(Cell right) {
        this.right = right;
    }

    public Cell getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Cell topLeft) {
        this.topLeft = topLeft;
    }

    public Cell getTopRight() {
        return topRight;
    }

    public void setTopRight(Cell topRight) {
        this.topRight = topRight;
    }

    public Cell getBtmLeft() {
        return btmLeft;
    }

    public void setBtmLeft(Cell btmLeft) {
        this.btmLeft = btmLeft;
    }

    public Cell getBtmRight() {
        return btmRight;
    }

    public void setBtmRight(Cell btmRight) {
        this.btmRight = btmRight;
    }
}
