package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import data.GameData;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import javax.swing.text.View;
import java.io.IOException;
import java.util.*;

import static buzzword.BuzzwordProperties.*;

/**
 * This class serves as the GUI component for the Buzzword game.
 *
 * @author Serge Crane
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI gui; // the GUI inside which the application sits

    Label guiHeadingLabel;        // workspace (GUI) heading label
    Label levelLabel;  // labels for the footToolbar component
    HBox headPane;               // container to display the heading "Buzzword"
    VBox middleContainer;        // container for the main game displays
    HBox footToolbar;            // bottom toolbar
    GridPane mainAppContainer;    // Main container for layout of the app
    GridPane gridPane;               // main game screen
    ToolBar menuContainer;          // container for left menus
    VBox gameTextsPane;          // container to display the text-related parts of the game
    GridPane guessedWordsContainer;           // words already guessed container
    HBox guessedLettersContainer;         // text area displaying all the letters currently
    HBox targetScoreBoxContainer;         // container to display the target score
    Button startGame;              // the button to start playing a game of Buzzword
    BuzzwordController controller;
    Button newProfileButton, loginButton, logoutButton, homeButton, pauseButton;
    HBox menuTitleBox;
    ComboBox modeSelectBox;          // box for mode selection
    VBox levelScreenContainer;
    FlowPane levelScreen, guessedLetters;
    Stage loginStage;
    Label titleLabel;
    StringBuffer currentSelection = new StringBuffer();
    VBox wordListVBox, scoreVBox;
    HBox totalScoreBox;

    public Label timerLabel = new Label();
    String wordSoFar = "";


    public String currentMode;


    boolean firstKeypress = true;


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzwordController) gui.getFileController();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        guiHeadingLabel.setStyle("-fx-font-size: 30;" +
                "-fx-font-weight: bold");
        //Buzzword Header
        //headPane = new HBox(guiHeadingLabel);
        HBox headingLabelContainer = new HBox(guiHeadingLabel);
        headingLabelContainer.setAlignment(Pos.CENTER);

        //headingLabelContainer.setPadding(new Insets(0,0,0,100));
        menuTitleBox = new HBox(new Label("Menu"));
        menuTitleBox.setMinWidth(300);
        menuTitleBox.setPadding(new Insets(0, 0, 0, 115));
        menuTitleBox.setStyle("-fx-background-color: LightGray;" +
                "-fx-font-size: 24;" +
                "-fx-font-weight: bold");
        //headPane.getChildren().addAll(menuTitleBox, headingLabelContainer);
        //headPane.setSpacing(290);

        mainAppContainer = new GridPane();
        drawGrid();

        newProfileButton = new Button("Create New Profile");
        newProfileButton.setPrefSize(285, 25);
        loginButton = new Button("Login");
        loginButton.setPrefSize(285, 25);

        menuContainer = new ToolBar(newProfileButton, loginButton);
        menuContainer.setOrientation(Orientation.VERTICAL);
        menuContainer.setStyle("-fx-background-color: LightGray");
        menuContainer.setPrefSize(300, 950);


        levelLabel = new Label("Level ");

        footToolbar = new HBox();
        footToolbar.setSpacing(600);
        //footToolbar.getChildren().addAll(levelLabel, timeLabel);

        workspace = new VBox();

        //mainAppContainer.setGridLinesVisible(true);
        mainAppContainer.setVgap(5);
        mainAppContainer.setHgap(5);
        gameTextsPane = new VBox();
        // col row colspan rowspan
        mainAppContainer.add(menuContainer, 0, 1, 1, 2);
        mainAppContainer.add(menuTitleBox, 0, 0, 1, 2);
        mainAppContainer.add(headingLabelContainer, 3, 0);
        //mainAppContainer.add(gridPane, 3, 2);  // dependency added when grid is drawn
        mainAppContainer.add(gameTextsPane, 5, 2);

        workspace.getChildren().add(mainAppContainer);


        controller.start();
    }

    public void openProfileCreation() {
        Stage profileStage = new Stage();
        BorderPane profilePane = new BorderPane();
        profilePane.setStyle("-fx-background-color: LightGray");
        profilePane.setPrefSize(400, 150);

        TextField unTextField = new TextField();
        unTextField.setPromptText("Enter desired username");
        unTextField.setFocusTraversable(false);

        PasswordField pwField = new PasswordField();
        pwField.setPromptText("Enter desired password");
        pwField.setFocusTraversable(false);
        PasswordField pwField2 = new PasswordField();
        pwField2.setPromptText("Confirm password");
        pwField2.setFocusTraversable(false);

        //Button
        Button loginBtn = new Button("Create Account");
        loginBtn.setOnAction(event -> {
            GameData data = (GameData) app.getDataComponent();
            // check password matching first
            if (pwField.getText().equals(pwField2.getText())) {
                if (!data.getAccountsMap().containsKey(unTextField.getText())) {
                    // digest the password before assigning it to account
                    String newPw = controller.digestPassword(pwField.getText());
                    System.out.println("Digested PW is: " + newPw);
                    controller.createNewProfile(unTextField.getText(), newPw);
                    profileStage.hide();
                } else {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Fail", "User already exists.");
                    pwField.clear();
                    pwField2.clear();
                }
            } else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Fail", "Passwords must match. Try again.");
                pwField.clear();
                pwField2.clear();
            }
        });

        HBox loginBtnContainer = new HBox(15);
        loginBtnContainer.setAlignment(Pos.BOTTOM_RIGHT);
        loginBtnContainer.getChildren().add(loginBtn);
        VBox centerContainer = new VBox(unTextField, pwField, pwField2, loginBtnContainer);
        centerContainer.setSpacing(4);
        profilePane.setCenter(centerContainer);
        profilePane.setPadding(new Insets(15, 15, 15, 15));
        Scene profileScene = new Scene(profilePane);
        profileStage.setTitle("Create New Account");
        profileStage.setScene(profileScene);
        profileStage.show();
    }

    public void openProfileEdit(){
        GameData data = (GameData) app.getDataComponent();
        Stage editProfileStage = new Stage();
        BorderPane profilePane = new BorderPane();
        profilePane.setStyle("-fx-background-color: LightGray");
        profilePane.setPrefSize(400, 150);

        Label usernameLabel = new Label(data.getUserAccount().getUsername() + " is logged in.");
        usernameLabel.setStyle("-fx-font-size: 16");


        PasswordField pwField = new PasswordField();
        pwField.setPromptText("Enter desired password");
        pwField.setFocusTraversable(false);
        PasswordField confirmPwField = new PasswordField();
        confirmPwField.setPromptText("Confirm old password");
        confirmPwField.setFocusTraversable(false);

        //Button
        Button changePwButton = new Button("Change Password");
        changePwButton.setOnAction(event -> {
            // check password matching first
            String oldPwDigested = data.getUserAccount().getPassword();
            String newDigestedPw = confirmPwField.getText();
            newDigestedPw = controller.digestPassword(newDigestedPw);
            if (oldPwDigested.equals(newDigestedPw)) {
                data.getUserAccount().setPassword(controller.digestPassword(pwField.getText()));

                //saving
                try {
                    controller.handleSaveRequest();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Success", "Password Changed");
                editProfileStage.hide();
            } else {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Fail", "Passwords do not match.");
                    pwField.clear();
                    confirmPwField.clear();
                }
        });

        HBox loginBtnContainer = new HBox(15);
        loginBtnContainer.setAlignment(Pos.BOTTOM_RIGHT);
        loginBtnContainer.getChildren().add(changePwButton);
        VBox centerContainer = new VBox(usernameLabel, pwField, confirmPwField, loginBtnContainer);
        centerContainer.setSpacing(4);
        profilePane.setCenter(centerContainer);
        profilePane.setPadding(new Insets(15, 15, 15, 15));
        Scene profileScene = new Scene(profilePane);
        editProfileStage.setTitle("View/Edit Account");
        editProfileStage.setScene(profileScene);
        editProfileStage.show();
    }

    public void drawGrid() {

        if (gridPane != null) {
            gridPane.getChildren().clear(); // MAYBEX
            mainAppContainer.getChildren().remove(gridPane);
        }


        gridPane = new GridPane();
        gridPane.setHgap(30);
        gridPane.setVgap(30);
        gridPane.setPadding(new Insets(50, 50, 50, 50));
        gridPane.setOnMouseDragExited(event -> {
            //controller.checkWordValidity();
            currentSelection = new StringBuffer();
            removeHighlighting();
        });

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Circle toAdd = new Circle(40, Color.LIGHTBLUE);
                Cell newCell;
                if (i == 0 && j == 0)
                    gridPane.add(writeLetter("B", toAdd), i, j);
                else if (i == 1 && j == 0)
                    gridPane.add(writeLetter("U", toAdd), i, j);
                else if (i == 0 && j == 1)
                    gridPane.add(writeLetter("Z", toAdd), i, j);
                else if (i == 1 && j == 1)
                    gridPane.add(writeLetter("Z", toAdd), i, j);
                else if (i == 2 && j == 2)
                    gridPane.add(writeLetter("W", toAdd), i, j);
                else if (i == 3 && j == 2)
                    gridPane.add(writeLetter("O", toAdd), i, j);
                else if (i == 2 && j == 3)
                    gridPane.add(writeLetter("R", toAdd), i, j);
                else if (i == 3 && j == 3)
                    gridPane.add(writeLetter("D", toAdd), i, j);
                else
                    gridPane.add(writeLetter(" ", toAdd), i, j);

                controller.numOfEmptyCircles--;
            }
        }

        mainAppContainer.add(gridPane, 3, 2);

    }

    public void highLightPane(Pane pane) {
        DropShadow glow = new DropShadow(20, Color.BLACK);
        pane.setEffect(glow);
    }

    public void highLightByPanes(ArrayList<StackPane> arrayList) {
        Iterator paneIterator = arrayList.iterator();
        while (paneIterator.hasNext()) {
            StackPane pane = (StackPane) paneIterator.next();
            DropShadow glow = new DropShadow(20, Color.BLACK);
            pane.setEffect(glow);
        }
    }

    public void highLightByCoordList(ArrayList<int[]> listOfPanes) {
        Iterator listOfPanesIterator = listOfPanes.iterator();

        while (listOfPanesIterator.hasNext()) {
            int[] coords = (int[]) listOfPanesIterator.next();
            StackPane pane = getStackPaneByColRow(coords[0], coords[1]);
            DropShadow glow = new DropShadow(20, Color.BLACK);
            pane.setEffect(glow);
        }
    }

    public void removeHighlighting() {
        ObservableList children = gridPane.getChildren();
        StackPane currentPane;

        for (int i = 0; i < children.size(); i++) {
            currentPane = (StackPane) children.get(i);
            currentPane.setEffect(null);
            currentPane.setMouseTransparent(false);
        }
    }

    public void setCircleText(int col, int row, String text) {
        System.out.println("col: " + col + " and row: " + row + "  text: " + text);
        Text t = new Text(text);
        t.setStyle(
                "-fx-font-size: 20;" +
                        "-fx-font-color: white"
        );
        t.setBoundsType(TextBoundsType.VISUAL);
        StackPane tempPane = getStackPaneByColRow(col, row);
        Circle oldCircle = (Circle) tempPane.getChildren().get(0);

        initDraggingHandlers(tempPane);
        initTypingHandlers(tempPane);

        tempPane.getChildren().setAll(oldCircle, t);
        gridPane.getChildren().remove(getStackPaneByColRow(col, row));  // THIS REMOVES SO NO OVERLAPPING DUPLICATES
        gridPane.add(tempPane, col, row);

        controller.numOfEmptyCircles--;
    }

    private void initDraggingHandlers(StackPane pane) {
        pane.setOnMouseDragEntered((MouseDragEvent event) -> {
            pane.setMouseTransparent(true);
            highLightPane(pane);

            if (currentSelection.length() >= 0) {
                currentSelection.append(getTextByPane(pane));
                Label ltrToAdd = new Label(getTextByPane(pane));
                guessedLetters.getChildren().add(ltrToAdd);
            }
        });

        pane.setOnDragDetected(event -> {
            pane.setMouseTransparent(true);
            pane.startFullDrag();
            highLightPane(pane);
            guessedLetters.getChildren().clear();
            if (currentSelection.length() >= 0) {
                currentSelection.append(getTextByPane(pane));
                Label ltrToAdd = new Label(getTextByPane(pane));
                guessedLetters.getChildren().add(ltrToAdd);
            }
        });

        pane.setOnMouseReleased(event -> {
            pane.setMouseTransparent(false);
            //if(!currentSelection.toString().equals(controller.currentlySelectedWord) && currentSelection != null && !currentSelection.toString().equals(""))
            controller.setSelection(currentSelection);
            controller.checkWordValidity();
            currentSelection = new StringBuffer();
            removeHighlighting();

        });
    }

    public ArrayList<StackPane> listCoordsToStackpanes(ArrayList<int[]> toConvert) {
        ArrayList<StackPane> toReturn = new ArrayList<>();
        for (int[] cellCoords : toConvert) {
            toReturn.add(getStackPaneByColRow(cellCoords[0], cellCoords[1]));
        }
        return toReturn;
    }

    private void initTypingHandlers(StackPane pane) {
        // scene receives keypresses, so must bind to scene
        gui.getPrimaryScene().onKeyPressedProperty().bind(pane.onKeyPressedProperty());

        pane.setOnKeyPressed(event -> {
            if (event.getCode().isLetterKey()) {
                unvisitCells();
                String letterTyped = event.getText();
                System.out.print(letterTyped);
                currentSelection.append(letterTyped);
                wordSoFar = wordSoFar + letterTyped;
                removeHighlighting();
                Label ltrToAdd = new Label(letterTyped);
                guessedLetters.getChildren().add(ltrToAdd);
                highlightPathByWord(wordSoFar);
            } else if (event.getCode() == KeyCode.ENTER) {
                guessedLetters.getChildren().clear();
                controller.setSelection(currentSelection);
                unvisitCells();
                firstKeypress = true;
                wordSoFar = "";
                currentSelection = new StringBuffer();
                removeHighlighting();
                controller.checkWordValidity();
            }
        });
    }

    private ArrayList<StackPane> getVisitedNodes(){
        ObservableList children = gridPane.getChildren();
        ArrayList<StackPane> listToReturn = new ArrayList<>();
        Cell currentCell;
        for(int i = 0; i < children.size(); i++){
            currentCell = (Cell) children.get(i);
            if(currentCell.isVisited()){
                listToReturn.add(currentCell);
            }
        }
        return listToReturn;
    }
    private Circle getCircleByPane(StackPane pane) {
        return (Circle) pane.getChildren().get(0);
    }

    public String getTextByPane(StackPane pane){
        Text text = (Text) pane.getChildren().get(1);
        return text.getText();
    }

    public void setListVisited(ArrayList<StackPane> paneList){
        for(StackPane pane: paneList){
            ((Cell)pane).setVisited(true);
        }
    }

    public StackPane getStackPaneByColRow(int col, int row){
        ObservableList<Node> children = gridPane.getChildren();
        for (Node node : children) {
            if(gridPane.getRowIndex(node) == row && gridPane.getColumnIndex(node) == col) {
                StackPane toReturn = (StackPane) node;
                return toReturn;
            }
        }
        System.out.println("Returning null for stackpane");
        return null;
    }

    public Circle getCircle(StackPane pane) {
        return (Circle) pane.getChildren().get(0);
    }

    /**
     * precondition: letter must be a valid letter on the board
     */
    public StackPane getStackPaneByText(String letter){
        ObservableList listOfNodes = gridPane.getChildren();
        Iterator iterator = listOfNodes.iterator();
        StackPane currentPane;
        while(iterator.hasNext()){
            currentPane = (StackPane)iterator.next();
            if(getTextByPane(currentPane).equals(letter)){
                return currentPane;
            }
        }
        return null;
    }

    public Stack<StackPane> getStackPanesByText(String letter){
        Stack<StackPane> toReturn = new Stack<>();
        ObservableList listOfNodes = gridPane.getChildren();
        Iterator iterator = listOfNodes.iterator();
        StackPane currentPane;
        while(iterator.hasNext()){
            currentPane = (StackPane)iterator.next();
            if(getTextByPane(currentPane).equals(letter)){
                toReturn.push(currentPane);
            }
        }
        return toReturn;
    }

    public int[] getStackPaneCoords(StackPane pane){
        ObservableList<Node> children = gridPane.getChildren();
        for (Node node : children) {
            if((gridPane.getRowIndex(pane) == gridPane.getRowIndex(node)) &&
                    (gridPane.getColumnIndex(node) == gridPane.getColumnIndex(pane))) {
                return new int[] {gridPane.getColumnIndex(node), gridPane.getRowIndex(node)};
            }
        }
        return null;
    }

    public boolean highlightPathByWord(String word){
        if(word.length() == 1){
            ArrayList<StackPane> arrayList = new ArrayList<>();
            arrayList.addAll(getStackPanesByText("" + word.charAt(0)));
            highLightByPanes(arrayList);
            return true;
        }
        // if we are here, then word length at least
        Stack<StackPane> possibleStartingCells = getStackPanesByText("" + word.charAt(0));
        boolean atLeastOnePath = false;
        while (!possibleStartingCells.isEmpty()) {
            //unvisitCellsExcept((Cell)possibleStartingCells.peek());
            if(tracePath2(0, word.toCharArray(), (Cell)possibleStartingCells.pop()))
                atLeastOnePath = true;
        }
        return atLeastOnePath;
    }

    public boolean tracePath2(int index, char[] letters, Cell currentCell){
        if(index == letters.length - 1){    // if index = last letter of word
            currentCell.setVisited(true);
            highLightPane(currentCell);
            return true;
        }
        currentCell.setVisited(true);
        highLightPane(currentCell);

        String nextLetter = "" + letters[index+1];
        ArrayList<int[]> validCoordsForCurrentCell = currentCell.getValidNeighborsNoVisits(nextLetter);
        Cell nextCell;

        if(validCoordsForCurrentCell.get(0)[0] == -1){                         // if no valid coords, no paths from this node
            currentCell.setVisited(false);
            currentCell.setEffect(null);
            return false;
        } else {
            // take each path
            for (int[] coords : validCoordsForCurrentCell) {
                nextCell = (Cell) getStackPaneByColRow(coords[0], coords[1]);
                nextCell.setVisited(true);
                highLightPane(nextCell);
                return tracePath2(index+1, letters, nextCell);
            }
        }
        return false; // stops the compile-time error but doesn't affect code.
    }
    public boolean tracePath(int index, char[] letters, Cell prevCell){
        if(index == letters.length){                                    // if index == length of word, done
            return true;
        }

        prevCell.setVisited(true);
        highLightPane(prevCell);

        String nextLetter = "" + letters[index];
        ArrayList<int[]> validCoords = prevCell.getValidNeighbors(nextLetter);
        Cell nextCell;

        if(validCoords.get(0)[0] == -1){                         // if no valid coords, no paths from this node
            prevCell.setVisited(false);
            prevCell.setEffect(null);
            return false;
        } else {
            // take each path
            for (int[] coords : validCoords) {
                nextCell = (Cell) getStackPaneByColRow(coords[0], coords[1]);
                nextCell.setVisited(true);
                highLightPane(nextCell);
                return tracePath(index+1, letters, nextCell);
            }
        }
        return false; // stops the compile-time error but doesn't affect code.
    }
    public void unvisitCells(){
        ObservableList<Node> children = gridPane.getChildren();
        Cell currentCell;
        for (Node node : children) {
            currentCell = (Cell) node;
            currentCell.setVisited(false);
        }
    }
    public void unvisitCellsExcept(Cell paneToExclude){
        ObservableList<Node> children = gridPane.getChildren();
        Cell currentCell;
        for (Node node : children) {
            currentCell = (Cell) node;
            if(currentCell.getRow() == paneToExclude.getRow() && currentCell.getCol() == paneToExclude.getCol()) {
                currentCell.setVisited(true);
            } else {
                currentCell.setVisited(false);
            }
        }
    }

    public void initCells(){
        Cell currentCell;
        StackPane tempPane;
        for (int col = 0; col < 4; col++){
            for (int row = 0; row < 4; row++){
                tempPane = getStackPaneByColRow(col,row);
                currentCell = (Cell) tempPane;
                currentCell.setCol(col);
                currentCell.setRow(row);
                currentCell.setLetter(getTextByPane(tempPane));

                if(col >= 1 && col <= 2 && row >= 1 && row <= 2){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(col - 1,row - 1));
                    currentCell.setTop((Cell)getStackPaneByColRow(col,row - 1));
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(col + 1,row - 1));
                    currentCell.setLeft((Cell)getStackPaneByColRow(col - 1, row));
                    currentCell.setRight((Cell)getStackPaneByColRow(col + 1, row));
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(col - 1, row + 1));
                    currentCell.setBtm((Cell)getStackPaneByColRow(col, row + 1));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(col + 1, row + 1));
                } else if(col == 0 && row == 0){
                    currentCell.setTopLeft(null);
                    currentCell.setTop(null);
                    currentCell.setTopRight(null);
                    currentCell.setLeft(null);
                    currentCell.setRight((Cell)getStackPaneByColRow(1,0));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm((Cell)getStackPaneByColRow(0,1));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(1,1));
                } else if (col == 0 && row == 1){
                    currentCell.setTopLeft(null);
                    currentCell.setTop((Cell)getStackPaneByColRow(0,0));
                    currentCell.setTopRight((Cell)getStackPaneByColRow(1,0));
                    currentCell.setLeft(null);
                    currentCell.setRight((Cell)getStackPaneByColRow(1,1));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm((Cell)getStackPaneByColRow(0,2));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(1,2));
                } else if (col == 0 && row == 2){
                    currentCell.setTopLeft(null);
                    currentCell.setTop((Cell)getStackPaneByColRow(0,1));
                    currentCell.setTopRight((Cell)getStackPaneByColRow(1,0));
                    currentCell.setLeft(null);
                    currentCell.setRight((Cell)getStackPaneByColRow(1,2));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm((Cell)getStackPaneByColRow(0,3));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(1,3));
                } else if (col == 0 && row == 3){
                    currentCell.setTopLeft(null);
                    currentCell.setTop((Cell)getStackPaneByColRow(0,2));
                    currentCell.setTopRight((Cell)getStackPaneByColRow(1,2));
                    currentCell.setLeft(null);
                    currentCell.setRight((Cell)getStackPaneByColRow(1,3));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm(null);
                    currentCell.setBtmRight(null);
                } else if (col == 1 && row == 0){
                    currentCell.setTopLeft(null);
                    currentCell.setTop(null);
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(0,0));
                    currentCell.setRight((Cell)getStackPaneByColRow(2,0));
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(2,1));
                    currentCell.setBtm((Cell)getStackPaneByColRow(1,1));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(0,1));
                } else if (col == 1 && row == 3){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(0, 2));
                    currentCell.setTop((Cell)getStackPaneByColRow(1, 2));
                    currentCell.setTopRight((Cell)getStackPaneByColRow(2, 2));
                    currentCell.setLeft((Cell)getStackPaneByColRow(0, 3));
                    currentCell.setRight((Cell)getStackPaneByColRow(2, 3));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm(null);
                    currentCell.setBtmRight(null);
                } else if (col == 2 && row == 0){
                    currentCell.setTopLeft(null);
                    currentCell.setTop(null);
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(1, 0));
                    currentCell.setRight((Cell)getStackPaneByColRow(3, 0));
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(1, 1));
                    currentCell.setBtm((Cell)getStackPaneByColRow(2, 1));
                    currentCell.setBtmRight((Cell)getStackPaneByColRow(3, 1));
                } else if (col == 2 && row == 3){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(1, 2));
                    currentCell.setTop((Cell)getStackPaneByColRow(2, 2));
                    currentCell.setTopRight((Cell)getStackPaneByColRow(3, 2));
                    currentCell.setLeft((Cell)getStackPaneByColRow(1, 3));
                    currentCell.setRight((Cell)getStackPaneByColRow(3, 3));
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm(null);
                    currentCell.setBtmRight(null);
                } else if (col == 3 && row == 0){
                    currentCell.setTopLeft(null);
                    currentCell.setTop(null);
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(2, 0));
                    currentCell.setRight(null);
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(2, 1));
                    currentCell.setBtm((Cell)getStackPaneByColRow(3, 1));
                    currentCell.setBtmRight(null);
                } else if (col == 3 && row == 1){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(2, 0));
                    currentCell.setTop((Cell)getStackPaneByColRow(3, 0));
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(2, 1));
                    currentCell.setRight(null);
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(2, 2));
                    currentCell.setBtm((Cell)getStackPaneByColRow(3, 2));
                    currentCell.setBtmRight(null);
                } else if (col == 3 && row == 2){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(2, 1));
                    currentCell.setTop((Cell)getStackPaneByColRow(3, 1));
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(2, 2));
                    currentCell.setRight(null);
                    currentCell.setBtmLeft((Cell)getStackPaneByColRow(2, 3));
                    currentCell.setBtm((Cell)getStackPaneByColRow(3, 3));
                    currentCell.setBtmRight(null);
                } else if (col == 3 && row == 3){
                    currentCell.setTopLeft((Cell)getStackPaneByColRow(2, 2));
                    currentCell.setTop((Cell)getStackPaneByColRow(3, 2));
                    currentCell.setTopRight(null);
                    currentCell.setLeft((Cell)getStackPaneByColRow(2, 3));
                    currentCell.setRight(null);
                    currentCell.setBtmLeft(null);
                    currentCell.setBtm(null);
                    currentCell.setBtmRight(null);
                }
            }
        }




    }


    public void setConfirmOnClose(boolean truth){
        if (truth) {
            gui.getWindow().setOnCloseRequest(event -> {
                Alert closeConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
                Button eButton = (Button) closeConfirmation.getDialogPane().lookupButton(ButtonType.OK);
                eButton.setText("Exit");
                closeConfirmation.setHeaderText("Are you sure you want to exit?");
                closeConfirmation.initOwner(gui.getWindow());

                Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
                if (!ButtonType.OK.equals(closeResponse.get())) {
                    event.consume();
                }
            });
        } else {
            gui.getWindow().setOnCloseRequest(event -> {
                    gui.getWindow().close();
            });
        }
    }

    public void addWordToScoreboard(String word, int value){
        Label wordLabel = new Label(word);
        Label scoreLabel = new Label("" + value);
        addLabelsToScoreboard(wordLabel, scoreLabel);
    }

    public void addLabelsToScoreboard(Label wordLabel, Label scoreLabel){
        wordListVBox.getChildren().add(wordLabel);
        scoreVBox.getChildren().add(scoreLabel);
    }
    public void setPauseButtonDisable (boolean enabled){
        pauseButton.setDisable(enabled);
    }
    public ArrayList<String> getScoreboardAsArrayList(){
        ArrayList<String> wordsOnBoard = new ArrayList<>();
        for(Node node : wordListVBox.getChildren()){
            String s = ((Label)node).getText();
            wordsOnBoard.add(s);
        }
        return wordsOnBoard;
    }
    public void updateTotal(boolean gameOver){
        int total = 0;
        ObservableList children = scoreVBox.getChildren();
        for (Object o : children){
            String valueString = ((Label)o).getText();
            int valueToAdd = Integer.parseInt(valueString);
            total += valueToAdd;
        }
        Label labelToAdd = new Label(""+total);
        if(gameOver)
            labelToAdd.setStyle("-fx-text-fill: red");
        totalScoreBox.getChildren().setAll(labelToAdd);
    }
    //gives error, Platform.runlater() this
    public int grabScoreFromLabel(){
        Iterator iterator = totalScoreBox.getChildren().iterator();
        for(Node node : totalScoreBox.getChildren()){
            Label tmpLabel = (Label)node;
            return Integer.parseInt(tmpLabel.getText());
        }
        return -1;
    }

    public void initGameplayScreen(int target){
        setConfirmOnClose(true); // prompts if need to exit

        // first remove remnants of levelselectscreen
        levelScreenContainer.getChildren().clear();
        gridPane.getChildren().clear();

        drawGrid();
        String temp = "";

        guessedLettersContainer = new HBox();
        targetScoreBoxContainer = new HBox();
        guessedWordsContainer = new GridPane();
        guessedLettersContainer.setMinWidth(350);
        guessedLettersContainer.setMaxWidth(350);
        gameTextsPane = new VBox();
        gameTextsPane.setSpacing(40);           //main spacing between components

        guessedLetters = new FlowPane();

        //guessedLetters.getChildren().addAll(bLabel,uLabel);
        guessedLetters.setHgap(5);
        guessedLetters.setMinHeight(25);
        guessedLettersContainer.setPadding(new Insets(0,97,0,0));
        guessedLettersContainer.getChildren().add(guessedLetters);
        highLightPane(guessedLettersContainer);

        //TImer
        Label timeRemainingLabel = new Label("Time Remaining");
        timeRemainingLabel.setStyle("-fx-font-size: 16; -fx-font-style: italic");

        HBox timerContainer = new HBox(timeRemainingLabel, timerLabel);
        timerContainer.setSpacing(10);
        timerContainer.setMaxWidth(325);
        timerContainer.setPadding(new Insets(0, 150, 0, 0));
        timerContainer.setAlignment(Pos.CENTER_LEFT);
        timerLabel.setStyle("-fx-font-style: italic; -fx-font-size: 26; -fx-font-color: White");
        //timerContainer.setStyle("-fx-background-color: LightGray");

        // The word box
        wordListVBox = new VBox();
        wordListVBox.setStyle("-fx-backgrond-color: LightGray");
        wordListVBox.setPadding(new Insets(0,0,0,5));
        VBox wordSeparator = new VBox();
        wordSeparator.setStyle("-fx-background-color: Gray");
        wordSeparator.setMinWidth(3);
        VBox totalSeparator = new VBox();
        totalSeparator.setStyle("-fx-background-color: Gray");
        totalSeparator.setMinWidth(3);
        scoreVBox = new VBox();
        scoreVBox.setPadding(new Insets(0,0,0,5));
        scoreVBox.setStyle("-fx-backgrond-color: LightGray");


        // Total and score box
        HBox totalHBox = new HBox();
        totalHBox.setStyle("-fx-background-color: LightGray");
        totalHBox.getChildren().addAll(new Label("TOTAL"));
        totalHBox.setPadding(new Insets(0,0,0,5));
        totalScoreBox = new HBox();
        totalScoreBox.setStyle("-fx-background-color: DarkGray");
        totalScoreBox.getChildren().addAll(new Label("" + 0));
        totalScoreBox.setPadding(new Insets(0,0,0,5));

        //sizes
        wordListVBox.setMinSize(150, 300);
        scoreVBox.setMinSize(100, 300);
        totalHBox.setMinSize(150, 20);
        totalScoreBox.setMinSize(100, 20);



        //scrollPane.setPrefSize(guessedWordsContainer.getWidth(), guessedWordsContainer.getHeight());


        guessedWordsContainer.add(wordListVBox, 0, 0);
        guessedWordsContainer.add(wordSeparator, 1, 0);
        guessedWordsContainer.add(scoreVBox, 2, 0);
        guessedWordsContainer.add(totalHBox, 0, 2);
        guessedWordsContainer.add(totalSeparator, 1, 2);
        guessedWordsContainer.add(totalScoreBox, 2, 2);

        ScrollPane scrollPane = new ScrollPane(scoreVBox);
        scrollPane.setFitToWidth(true);

        guessedWordsContainer.setPadding(new Insets(0,50,0,0));


        wordListVBox.setStyle("-fx-background-color: LightGray");
        scoreVBox.setStyle("-fx-background-color: LightGray");
        totalHBox.setStyle("-fx-background-color: DarkGray");


        // Target Score Box
        Label targetLabel = new Label("Target");
        //temp = (String)controller.wordsOnGrid.get(0);
        Label targetPoints = new Label("" + target);
        targetLabel.setStyle("-fx-font-weight: bold;" +
                        "-fx-font-size: 24"
        );
        targetPoints.setStyle("-fx-font-style: italic; -fx-font-size: 26");
        targetScoreBoxContainer.setSpacing(15);
        targetScoreBoxContainer.getChildren().addAll(targetLabel, targetPoints);

        HBox levelLabelContainer = new HBox(levelLabel);
        levelLabelContainer.setAlignment(Pos.CENTER);
        pauseButton = new Button("Pause");
        pauseButton.setPrefSize(75,50);
        HBox pauseButtonContainer = new HBox(pauseButton);
        pauseButtonContainer.setPadding(new Insets(30,0,0,0));
        pauseButton.setOnMouseClicked(event ->  {
            if (pauseButton.getText().equals("Pause")) {
                controller.pauseTimer();
                pauseButton.setText("Resume");
                gridPane.setVisible(false);
                Label pausedLabel = new Label("Game Paused");

                pausedLabel.setStyle("-fx-font-size:30");
                HBox pausedLabelContainer = new HBox(pausedLabel);
                pausedLabelContainer.setPadding(new Insets(225, 169, 240, 160));
                middleContainer.setMinWidth(pausedLabelContainer.getWidth());
                middleContainer.getChildren().set(1, pausedLabelContainer);
            }
            else if (pauseButton.getText().equals("Resume")) {
                controller.resumeTimer();
                pauseButton.setText("Pause");
                gridPane.setVisible(true);
                middleContainer.getChildren().set(1, gridPane);
            }
        });
        pauseButtonContainer.setAlignment(Pos.CENTER);
        middleContainer = new VBox();
        middleContainer.setSpacing(25);
        middleContainer.getChildren().setAll(levelLabelContainer, gridPane, pauseButtonContainer);
        gameTextsPane.getChildren().addAll(timerContainer, targetScoreBoxContainer, guessedLettersContainer, guessedWordsContainer);

        mainAppContainer.add(middleContainer, 3, 2);
        mainAppContainer.add(gameTextsPane, 5, 2);
        menuContainer.getItems().setAll(logoutButton, homeButton);

    }


    public void clearGrid(){
        gridPane.getChildren().clear(); // MAYBEX
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                Circle toAdd = new Circle(40, Color.LIGHTBLUE);
                    gridPane.add(writeLetter(" ", toAdd), i, j);
                // lines
            }
        }
        controller.numOfEmptyCircles = 16;
    }

    private Cell writeLetter(String s, Circle o){
        Text text = new Text(s);
        text.setStyle(
                "-fx-font-size: 20;" +
                        "-fx-font-color: white"
        );
        text.setBoundsType(TextBoundsType.VISUAL);
        Cell tempPane = new Cell();
        tempPane.setLetter(text.getText());
        tempPane.getChildren().addAll(o, text);
        return tempPane;
    }

    public void openLoginScreen(){
        loginStage = new Stage();
        BorderPane loginPane = new BorderPane();
        loginPane.setStyle("-fx-background-color: LightGray");
        loginPane.setPrefSize(200, 75);

        TextField unTextField = new TextField();
        unTextField.setPromptText("Username");
        unTextField.setFocusTraversable(false);

        PasswordField pwField = new PasswordField();
        pwField.setPromptText("Password");
        pwField.setFocusTraversable(false);

        //Button
        Button loginBtn = new Button("Log In");
        loginBtn.setOnAction(event -> {
            String user = unTextField.getText();
            String pw = pwField.getText();
            // here we have to init log in
            controller.authenticate(user, pw);
        });

        HBox loginBtnContainer = new HBox(15);
        loginBtnContainer.setAlignment(Pos.BOTTOM_RIGHT);
        loginBtnContainer.getChildren().add(loginBtn);
        VBox centerContainer = new VBox(unTextField,pwField,loginBtnContainer);
        centerContainer.setSpacing(3);
        loginPane.setCenter(centerContainer);
        loginPane.setPadding(new Insets(15,15,15,15));
        Scene loginScene = new Scene(loginPane);
        loginStage.setTitle("Login");
        loginStage.setScene(loginScene);
        loginStage.show();
    }
    private void setupHandlers() {
        //startGame.setOnMouseClicked(e -> controller.start());
        loginButton.setOnMouseClicked(event -> openLoginScreen());
        newProfileButton.setOnAction(event -> openProfileCreation());

    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public Button getStartGame() {
        return startGame;
    }


    public void displayLoss(){
        gridPane.setDisable(true);
        //Label textLabel = new Label("You Lost");
        //textLabel.setStyle("-fx-font-size:30");
        // HBox pausedLabelContainer = new HBox(textLabel);
        //pausedLabelContainer.setPadding(new Insets(225, 169, 240, 160));
        //middleContainer.getChildren().set(1, pausedLabelContainer);
    }

    public void displayWin(){
        //gridPane.setVisible(false);
        //controller.pauseTimer();
        AppMessageDialogSingleton dialogSingleton = AppMessageDialogSingleton.getSingleton();
        dialogSingleton.show("You Won!", "Your personal best is: " + ((GameData)app.getDataComponent()).getBestForLevel(currentMode));
    }



















    //###############################################################################
    public void openMainMenu(){
        GameData gamedata = (GameData) app.getDataComponent();

        Image exitImage = new Image("/images/Exit.png");
        // Username/login button
        logoutButton = new Button(gamedata.getUserAccount().getUsername(), new ImageView(exitImage));
        logoutButton.setPrefSize(285, 25);
        logoutButton.setAlignment(Pos.BASELINE_LEFT);
        logoutButton.setOnAction(event -> {
            controller.promptLogout();
        });

        // Select mode
        modeSelectBox = new ComboBox();
        modeSelectBox.setPromptText("Select Mode");
        modeSelectBox.getItems().setAll(
                "English Dictionary",
                "Places",
                "Science",
                "Popular Names"
        );
        modeSelectBox.setPrefSize(285,25);

        startGame = new Button("Start Playing");
        startGame.setPrefSize(285,25);
        startGame.setAlignment(Pos.BASELINE_LEFT);
        startGame.setOnMouseClicked(e -> {
            if (modeSelectBox.getSelectionModel().getSelectedItem() != null) {
                String selection = modeSelectBox.getSelectionModel().getSelectedItem().toString().trim();
                controller.setGameState(BuzzwordController.GameState.LEVEL_SELECT);
                loadLevel(selection);
            }
        });

        Button viewProfileBtn = new Button("View Profile");
        viewProfileBtn.setOnAction(e->openProfileEdit());
        viewProfileBtn.setMinSize(285,25);
        viewProfileBtn.setAlignment(Pos.BASELINE_LEFT);
        menuContainer.getItems().setAll(logoutButton, viewProfileBtn, modeSelectBox, startGame);
        loginStage.hide();
    }

    public void loadLevel(String mode){
        GameData gamedata = (GameData) app.getDataComponent();
        levelLabel = new Label("Level ");
        // levelScreenContainer goes in the middle
        levelScreenContainer = new VBox();
        levelScreenContainer.setMaxWidth(500);
        levelScreenContainer.setPadding(new Insets(50));
        titleLabel = new Label(mode);
        levelScreen = new FlowPane();
        levelScreen.setHgap(25);
        levelScreen.setVgap(25);
        levelLabel.setStyle("-fx-font-size: 22; -fx-font-weight: bold");
        //levelLabel.setPadding(new Insets(50, 50, 50, 50));
        currentMode = mode;
        switch (mode) {
            case "English Dictionary":
                levelLabel.setText(levelLabel.getText() + mode);
                controller.loadWords("words");
                for(int i = 0; i < 8; i++){
                    String levelNum = (i+1) + "";
                    if (i < gamedata.getCurrentLevelProgress()[0]){
                        StackPane circlePane = writeLetter(levelNum, new Circle(40, Color.LIGHTBLUE));
                        int lvlNum = i+1;       // level number
                        circlePane.setOnMouseClicked(event -> {
                            System.out.println("Loading " + mode + ". Level number " + lvlNum);
                            // generate grid calls init Gameplay
                            gamedata.setLevel(lvlNum);
                            initGameplayScreen(gamedata.getTargetScoreByLevel());
                            clearGrid();
                            controller.generateGrid();
                            //to reset
                            gridPane.setVisible(true);
                            middleContainer.getChildren().set(1, gridPane);

                            levelLabel.setText(mode + ", Level " + lvlNum);

                        });
                        levelScreen.getChildren().add(circlePane);
                    } else
                        levelScreen.getChildren().add(writeLetter(levelNum, new Circle(40, Color.LIGHTCORAL)));
                }
                break;
            case "Places":
                levelLabel.setText(levelLabel.getText() + mode);
                controller.loadWords(mode.toLowerCase());
                for(int i = 0; i < 8; i++){
                    String levelNum = (i+1) + "";
                    if (i < gamedata.getCurrentLevelProgress()[1]){
                        StackPane circlePane = writeLetter(levelNum, new Circle(40, Color.LIGHTBLUE));
                        int lvlNum = i+1;       // level number
                        circlePane.setOnMouseClicked(event -> {
                            System.out.println("Loading " + mode + ". Level number " + lvlNum);
                            // generate grid calls init Gameplay
                            gamedata.setLevel(lvlNum);
                            initGameplayScreen(gamedata.getTargetScoreByLevel());
                            clearGrid();
                            controller.generateGrid();
                            //to reset
                            gridPane.setVisible(true);
                            middleContainer.getChildren().set(1, gridPane);

                            levelLabel.setText(mode + ", Level " + lvlNum);
                        });
                        levelScreen.getChildren().add(circlePane);
                    } else
                        levelScreen.getChildren().add(writeLetter(levelNum, new Circle(40, Color.LIGHTCORAL)));
                }
                break;
            case "Science":
                levelLabel.setText(levelLabel.getText() + mode);
                controller.loadWords(mode.toLowerCase());
                for(int i = 0; i < 8; i++){
                    String levelNum = (i+1) + "";
                    if (i < gamedata.getCurrentLevelProgress()[2]){
                        StackPane circlePane = writeLetter(levelNum, new Circle(40, Color.LIGHTBLUE));
                        int lvlNum = i+1;       // level number
                        circlePane.setOnMouseClicked(event -> {
                            System.out.println("Loading " + mode + ". Level number " + lvlNum);
                            // generate grid calls init Gameplay
                            gamedata.setLevel(lvlNum);
                            initGameplayScreen(gamedata.getTargetScoreByLevel());
                            clearGrid();
                            controller.generateGrid();
                            //to reset
                            gridPane.setVisible(true);
                            middleContainer.getChildren().set(1, gridPane);

                            levelLabel.setText(mode + ", Level " + lvlNum);
                        });
                        levelScreen.getChildren().add(circlePane);
                    } else
                        levelScreen.getChildren().add(writeLetter(levelNum, new Circle(40, Color.LIGHTCORAL)));
                }
                break;
            case "Popular Names":
                levelLabel.setText(levelLabel.getText() + mode);
                controller.loadWords("names");
                for(int i = 0; i < 8; i++){
                    String levelNum = (i+1) + "";
                    if (i < gamedata.getCurrentLevelProgress()[1]){
                        StackPane circlePane = writeLetter(levelNum, new Circle(40, Color.LIGHTBLUE));
                        int lvlNum = i+1;       // level number
                        circlePane.setOnMouseClicked(event -> {
                            System.out.println("Loading " + mode + ". Level number " + lvlNum);
                            // generate grid calls init Gameplay
                            gamedata.setLevel(lvlNum);
                            initGameplayScreen(gamedata.getTargetScoreByLevel());
                            clearGrid();
                            controller.generateGrid();
                            //to reset
                            gridPane.setVisible(true);
                            middleContainer.getChildren().set(1, gridPane);

                            levelLabel.setText(mode + ", Level " + lvlNum);
                        });
                        levelScreen.getChildren().add(circlePane);
                    } else
                        levelScreen.getChildren().add(writeLetter(levelNum, new Circle(40, Color.LIGHTCORAL)));
                }
                break;
        }

        reinitialize();

    }

    public void clearSideContainers(){
        guessedWordsContainer.getChildren().clear();
        targetScoreBoxContainer.getChildren().clear();
        scoreVBox.getChildren().clear();
        //guessedWordsContainer.getChildren().clear();
    }


    public void reinitialize() {
        mainAppContainer.getChildren().clear();

        if (controller.getGamestate().toString().equals("LEVEL_SELECT")){
            HBox titleLabelContainer = new HBox();
            titleLabelContainer.setAlignment(Pos.CENTER);
            titleLabel.setStyle("-fx-font-size: 24");
            titleLabelContainer.getChildren().add(titleLabel);
            titleLabelContainer.setPadding(new Insets(15));
            levelScreenContainer.getChildren().addAll(titleLabelContainer, levelScreen);

            // enable home button hide the rest
            homeButton = new Button("Home");
            homeButton.setPrefSize(285, 25);
            homeButton.setAlignment(Pos.BASELINE_LEFT);
            homeButton.setOnAction(event -> {
                gridPane.getChildren().clear();
                setConfirmOnClose(false);
                controller.setGameState(BuzzwordController.GameState.MAIN_MENU);
                reinitialize();
            });
            menuContainer.getItems().setAll(logoutButton, homeButton);


            // redrawing elements
            HBox headingLabelContainer = new HBox(guiHeadingLabel);
            headingLabelContainer.setAlignment(Pos.CENTER);

            mainAppContainer.getChildren().clear();
            mainAppContainer.add(levelScreenContainer, 3, 2);
            mainAppContainer.add(menuContainer, 0, 1, 1, 2);
            mainAppContainer.add(menuTitleBox, 0, 0, 1, 2);
            mainAppContainer.add(headingLabelContainer, 3, 0);
            mainAppContainer.add(gameTextsPane, 5, 2);

            setupHandlers();
            controller.start();
        }
        if (controller.getGamestate().toString().equals("MAIN_MENU")){
            mainAppContainer.getChildren().clear();

            Button viewProfileBtn = new Button("View Profile");
            viewProfileBtn.setOnAction(e->openProfileEdit());
            viewProfileBtn.setMinSize(loginButton.getWidth(), logoutButton.getHeight());
            viewProfileBtn.setAlignment(Pos.BASELINE_LEFT);

            drawGrid();

            menuContainer.getItems().setAll(logoutButton, viewProfileBtn, modeSelectBox, startGame);
            gameTextsPane = new VBox();

            // redrawing elements
            HBox headingLabelContainer = new HBox(guiHeadingLabel);
            headingLabelContainer.setAlignment(Pos.CENTER);

            mainAppContainer.getChildren().clear();
            mainAppContainer.add(gridPane, 3, 2);
            mainAppContainer.add(menuContainer, 0, 1, 1, 2);
            mainAppContainer.add(menuTitleBox, 0, 0, 1, 2);
            mainAppContainer.add(headingLabelContainer, 3, 0);
            mainAppContainer.add(gameTextsPane, 5, 2);
            controller.start();
        }
        if (controller.getGamestate().toString().equals("GAMEPLAY")){
            clearSideContainers();

            //controller.startTimer();
            HBox headingLabelContainer = new HBox(guiHeadingLabel);
            headingLabelContainer.setAlignment(Pos.CENTER);

            menuContainer.setPadding(new Insets(50, 0, 0,0));
            menuContainer.getItems().setAll(logoutButton, homeButton);
            mainAppContainer.add(menuContainer, 0, 1, 1, 2);
            mainAppContainer.add(menuTitleBox, 0, 0, 1, 2);
            mainAppContainer.add(gridPane, 3, 2);

            initGameplayScreen(((GameData)app.getDataComponent()).getTargetScoreByLevel());
            clearGrid();

            for(Node node : gridPane.getChildren()){
                StackPane pane = (StackPane) node;
                initDraggingHandlers(pane);
            }

            controller.generateGrid();

        }
        if (controller.getGamestate().toString().equals("LOGGED_OUT")){
            menuTitleBox.getChildren().clear();

            PropertyManager propertyManager = PropertyManager.getManager();
            guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
            guiHeadingLabel.setStyle("-fx-font-size: 30;" +
                    "-fx-font-weight: bold");

            menuTitleBox = new HBox(new Label("Menu"));
            menuTitleBox.setMinWidth(300);
            menuTitleBox.setPadding(new Insets(0, 0, 0, 115));
            menuTitleBox.setStyle("-fx-background-color: LightGray;" +
                    "-fx-font-size: 24;" +
                    "-fx-font-weight: bold");
            HBox headingLabelContainer = new HBox(guiHeadingLabel);
            headingLabelContainer.setAlignment(Pos.CENTER);

            newProfileButton = new Button("Create New Profile");
            newProfileButton.setPrefSize(285, 25);
            loginButton = new Button("Login");
            loginButton.setPrefSize(285, 25);

            menuContainer = new ToolBar(newProfileButton, loginButton);
            menuContainer.setOrientation(Orientation.VERTICAL);
            menuContainer.setStyle("-fx-background-color: LightGray");
            menuContainer.setPrefSize(300, 950);

            mainAppContainer.add(menuContainer, 0, 1, 1, 2);
            mainAppContainer.add(menuTitleBox, 0, 0, 1, 2);
            mainAppContainer.add(headingLabelContainer, 3, 0);
            mainAppContainer.add(gridPane, 3, 2);
            mainAppContainer.add(gameTextsPane, 5, 2);

            setupHandlers();
            controller.start();
        }
    }

    public GridPane getGridPane(){
        return gridPane;
    }

}















