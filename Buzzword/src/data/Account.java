package data;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.ArrayList;

/**
 * Created by Serge on 11/27/2016.
 */
public class Account {
    private String username;
    private String password;

    private int[] levelProgress;
    private ArrayList<int[]> personalBestScores;

    @JsonCreator
    public Account(){

    }

    public Account(String username, String password, int[] levelProgress){
        this.username = username;
        this.password = password;
        this.levelProgress = levelProgress;
        this.personalBestScores = generateZeros();
    }

    public ArrayList<int[]> generateZeros(){
        ArrayList<int[]> allBestScores = new ArrayList<>();
        for(int mode = 0; mode < 4; mode++){
            int[] scoreArray = {0,0,0,0,0,0,0,0};
            allBestScores.add(scoreArray);
        }
        return allBestScores;
    }

    public ArrayList<int[]> getPersonalBestScores() {
        return personalBestScores;
    }

    public void setPersonalBestScores(ArrayList<int[]> personalBestScores) {
        this.personalBestScores = personalBestScores;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int[] getLevelProgress() {
        return levelProgress;
    }

    public void setLevelProgress(int[] levelProgress) {
        this.levelProgress = levelProgress;
    }
}
