package data;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import components.AppDataComponent;

import java.lang.reflect.Array;
import java.util.*;

/**
 * @author Serge Crane
 */
public class GameData implements AppDataComponent {
    private AppTemplate appTemplate;


    private LinkedHashMap<String, Account> accountsMap;

    private Account userAccount;
    private int[] currentLevelProgress;

    private String startingWord;
    private String wordFromUserInput;
    private HashSet<String> wordSet;

    private final int[] levelDifficulty = {25, 50, 75, 100, 150, 200, 300, 500};
    private final int   pointsPerLetter = 5;
    private       int   level;

    public int getBestForLevel(String mode){
        ArrayList<int[]> scores = userAccount.getPersonalBestScores();
        int[] modeScores;
        switch (mode) {
            case "English Dictionary":
                modeScores = scores.get(0);
                break;
            case "Places":
                modeScores = scores.get(1);
                break;
            case "Science":
                modeScores = scores.get(2);
                break;
            default:
                modeScores = scores.get(3);
                break;
        }
        return modeScores[level-1];
    }

    public void checkAndSetBestForLevel(String mode, int score){
        ArrayList<int[]> scores = userAccount.getPersonalBestScores();
        int[] modeScores;
        switch (mode) {
            case "English Dictionary":
                modeScores = scores.get(0);
                if(modeScores[level-1] < score)
                    modeScores[level-1] = score;
                break;
            case "Places":
                modeScores = scores.get(1);
                if(modeScores[level-1] < score)
                    modeScores[level-1] = score;
                break;
            case "Science":
                modeScores = scores.get(2);
                if(modeScores[level-1] < score)
                    modeScores[level-1] = score;
                break;
            default:
                modeScores = scores.get(3);
                if(modeScores[level-1] < score)
                    modeScores[level-1] = score;
                break;
        }
        userAccount.setPersonalBestScores(scores);
        accountsMap.put(userAccount.getUsername(), userAccount);
    }

    public void updateLevelProgress(String mode){
        switch (mode) {
            case "English Dictionary":
                currentLevelProgress[0] += 1;
                break;
            case "Places":
                currentLevelProgress[1] += 1;
                break;
            case "Science":
                currentLevelProgress[2] += 1;
                break;
            default:
                currentLevelProgress[3] += 1;
                break;
        }
        userAccount.setLevelProgress(currentLevelProgress);
        accountsMap.put(userAccount.getUsername(), userAccount);
    }
    public int getTargetScoreByLevel() {
        return levelDifficulty[level-1];
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            init(); // does the same as old code
        } else {
            this.appTemplate = appTemplate;
        }
    }



    public void init() {
        if (accountsMap == null)
            accountsMap = new LinkedHashMap<>();

    }


    @JsonAnyGetter
    public LinkedHashMap<String, Account> getAccountsMap() {
        return accountsMap;
    }

    public void setAccountsMap(LinkedHashMap<String, Account> accountsMap) {
        this.accountsMap = accountsMap;
    }

    @JsonAnySetter
    public void putAccountsMap(String key, Account value){
        if (accountsMap == null)
            accountsMap = new LinkedHashMap<>();
        accountsMap.put(key, value);
    }

    @Override
    public void reset() {
        userAccount = null;
        currentLevelProgress = null;
    }

    public int getPointsPerLetter() {
        return pointsPerLetter;
    }

    public String getWordFromUserInput() {
        return wordFromUserInput;
    }

    public void setWordFromUserInput(String wordFromUserInput) {
        this.wordFromUserInput = wordFromUserInput;
    }

    public Account getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(Account userAccount) {
        this.userAccount = userAccount;
    }

    public int[] getCurrentLevelProgress() {
        return currentLevelProgress;
    }

    public void setCurrentLevelProgress(int[] currentLevelProgress) {
        this.currentLevelProgress = currentLevelProgress;
    }

    public String getStartingWord() {
        return startingWord;
    }

    public void setStartingWord(String startingWord) {
        this.startingWord = startingWord;
    }

    public HashSet<String> getWordSet() {
        return wordSet;
    }

    public void setWordSet(List<String> wordSet){
        HashSet<String> tmpSet = new HashSet<>();
        Iterator<String> listIterator = wordSet.iterator();
        while (listIterator.hasNext()){
            tmpSet.add(listIterator.next().toLowerCase().replaceAll("[^A-Za-z]", ""));
        }
        this.wordSet = tmpSet;
    }

}
