package data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;


/**
 * @author Ritwik Banerjee
 */
public class GameDataFile implements AppFileComponent {
    ObjectMapper om = new ObjectMapper();


    File target;
    @Override
    public void saveData(AppDataComponent data, Path to) {
        GameData       gamedata    = (GameData) data;
        om.enable(SerializationFeature.INDENT_OUTPUT);
        target = new File(to.toString());
        Map map = gamedata.getAccountsMap();
        try {
            om.writeValue(target, map);
        } catch (IOException e) {
            System.out.println("Error saving accountMap");
        }
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData gamedata = (GameData) data;
        gamedata.reset();
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
